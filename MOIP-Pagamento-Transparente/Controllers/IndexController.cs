﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Net;
using System.Collections.Specialized;
using System.Text;
using System.IO;

namespace MOIP_Pagamento_Transparente.Controllers
{
    public class IndexController : Controller
    {
        public ActionResult Index()
        {
            string strXMLTransparente =
                 @"<EnviarInstrucao>
                <InstrucaoUnica TipoValidacao='Transparente'>
                    <Razao>Pagamento para loja X</Razao>
                    <Valores>
                        <Valor moeda='BRL'>1.00</Valor>
                    </Valores>
                    <IdProprio>" + (new Random()).Next(1, 10).ToString() +
                    @"</IdProprio>
                    <Pagador>
                        <Nome>Cliente Sobrenome</Nome>
                        <Email>login@meudominio.com.br</Email>
                        <IdPagador>MEU_CLIENTE_ID</IdPagador>
                        <EnderecoCobranca>
                            <Logradouro>Av. Brigadeiro Faria Lima</Logradouro>
                            <Numero>2927</Numero>
                            <Complemento>8° Andar</Complemento>
                            <Bairro>Jardim Paulistao</Bairro>
                            <Cidade>Sao Paulo</Cidade>
                            <Estado>SP</Estado>
                            <Pais>BRA</Pais>
                            <CEP>01452-000</CEP>
                            <TelefoneFixo>(11)3165-4020</TelefoneFixo>
                        </EnderecoCobranca>
                    </Pagador>
                    <Parcelamentos>
                        <Parcelamento>
                            <MinimoParcelas>2</MinimoParcelas>
                            <MaximoParcelas>2</MaximoParcelas>
                            <Juros>1.99</Juros>
                        </Parcelamento>
                        <Parcelamento>
                            <MinimoParcelas>3</MinimoParcelas>
                            <MaximoParcelas>4</MaximoParcelas>
                            <Juros>0</Juros>                
                        </Parcelamento>
                        <Parcelamento>
                            <MinimoParcelas>5</MinimoParcelas>
                            <MaximoParcelas>6</MaximoParcelas>
                            <Repassar>true</Repassar>
                        </Parcelamento>
                        <Parcelamento>
                            <MinimoParcelas>7</MinimoParcelas>
                            <MaximoParcelas>12</MaximoParcelas>
                            <Juros>10.00</Juros>
                        </Parcelamento>
                    </Parcelamentos>
              </InstrucaoUnica>
            </EnviarInstrucao>";

            var xml = sendPost(strXMLTransparente);

            var status = xml.DocumentElement.SelectSingleNode("//Resposta//Status").InnerText;
            var token = xml.DocumentElement.SelectSingleNode("//Resposta//Token").InnerText;

            ViewBag.status = status;
            ViewBag.token = token;

            return View();
        }

        public XmlDocument sendPost(string XMLInstrucao)
        {
            WebClient client = new WebClient();
            
            string Token = "myToken";
            string Key = "myKey";
          
            //URL do Sandbox
            string URI = "https://desenvolvedor.moip.com.br/sandbox/ws/alpha/EnviarInstrucao/Unica";
            string auth = Convert.ToBase64String(Encoding.UTF8.GetBytes(Token + ":" + Key));

            client.Headers.Add("Authorization: Basic " + auth);
            client.Headers.Add("User-Agent: Mozilla/4.0");

            byte[] ResponseArray = client.UploadData(URI, "POST", Encoding.UTF8.GetBytes(XMLInstrucao));
            string Response = Encoding.UTF8.GetString(ResponseArray);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(Response);
           
            return xml;
        }
    }
    
}
